fastapi==0.111.1
uvicorn==0.30.1
arrow==1.3.0
pydantic==2.8.2
otter_grader_service --extra-index-url=https://gitlab.ethz.ch/api/v4/projects/45207/packages/pypi/simple
