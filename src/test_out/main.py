from fastapi import FastAPI, Request, Depends, Header
import urllib.parse
import json
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from typing import Annotated
import uvicorn
import arrow
import time
from otter_grader_service.grading.worker import GRADING_OK, MULTIPLE_NOTEBOOKS, NO_NOTEBOOK

app = FastAPI()
test_date = "No result yet"
test_timestamp = 0


class Response(BaseModel):
    submission_id: int
    rc: int
    version: str
    ts: int


class Test:
    job_id: int
    expected_result: int
    received_result: int = -1
    timestamp: int = 0
    grader_version: str = "No result yet"

    def __init__(self, job_id):
        self.job_id = job_id
        if job_id == 1:
            self.expected_result = NO_NOTEBOOK
        if job_id == 2:
            self.expected_result = GRADING_OK
        if job_id == 3:
            self.expected_result = MULTIPLE_NOTEBOOKS

    def passed(self):
        return self.expected_result == self.received_result


tests = {}
for i in range(1, 4):
    tests[i] = Test(i)


def result():
    res = {}
    passed = True
    for job_id in tests.keys():
        test_result = tests[job_id].passed()
        if not test_result:
            passed = False
        res[job_id] = int(test_result)
    res['summary'] = int(passed)
    res['version'] = tests[3].grader_version
    res['date'] = test_date
    res['timestamp'] = test_timestamp
    return res


async def get_body(request: Request):
    return await request.body()


@app.post("/push")
async def get_results(body: bytes = Depends(get_body), access_token: Annotated[str | None, Header()] = None):
    global test_date
    global test_timestamp
    if access_token is None:
        print("No out token")
    else:
        print(f"Out token: {access_token}")
    decoded = urllib.parse.parse_qs(body)
    input_str = decoded[b"input"][0]
    args = json.loads(urllib.parse.unquote(input_str))
    submission_id = int(args["submission_id"])
    tests[submission_id] = Test(submission_id)
    tests[submission_id].received_result = int(args["rc"])
    tests[submission_id].grader_version = str(args["version"])
    tests[submission_id].timestamp = int(args["ts"])
    print(f"Got id={submission_id}, rc={tests[submission_id].received_result}")
    if submission_id == 1:
        if tests[submission_id].received_result == NO_NOTEBOOK:
            print("Missing notebook detected")
        else:
            print("Missing notebook NOT detected")

    if submission_id == 2:
        if tests[submission_id].received_result == GRADING_OK:
            print("Grading detected")
        else:
            print("Grading NOT detected")

    if submission_id == 3:
        if tests[submission_id].received_result == MULTIPLE_NOTEBOOKS:
            print("Multiple notebooks detected")
        else:
            print("Multiple notebooks NOT detected")

    if submission_id not in [1, 2, 3]:
        print("Unknown test case")

    test_date = arrow.now().format('YYYY-MM-DD HH:mm:ss')
    test_timestamp = time.time()


@app.get("/status")
async def status():
    return result()


@app.get("/", response_class=HTMLResponse)
async def index():
    res = result()
    if res["summary"]:
        summary = "All tests have passed:"
    else:
        summary = "Test failed:"
    return f"""
    <html>
        <head>
            <title>otter-grader-service Test Results</title>
        </head>
        <body>
            <div>{summary} <a href="/status">otter-grader-service</a></div>
            <div>Test valid for version {res['version']} at {res['date']}</div>
        </body>
    </html>
    """


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)
